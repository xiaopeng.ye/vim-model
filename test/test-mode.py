import pynvim

n = pynvim.attach(
    "child", argv=['/bin/env', 'nvim', '--clean', '--embed', '--headless', 'filename'])

n.input(":set cmdheight=2<CR>")

# Problem: Consecutive call of get_mode() does not return the same mode
n.feedkeys(n.replace_termcodes("gh"), 't')
assert (n.api.get_mode() == dict(
    {'mode': 's', 'blocking': False})), "not in select mode or is blocked!"
assert (n.api.get_mode() == dict(
    {'mode': 's', 'blocking': False})), "not in select mode or is blocked!"

n.feedkeys(n.replace_termcodes("<C-o>"), 't')
assert (n.api.get_mode() == dict(
    {'mode': 'vs', 'blocking': False})), "not in select visual mode or is blocked!"
assert (n.api.get_mode() == dict(
    {'mode': 'vs', 'blocking': False})), "not in select visual mode or is blocked!"

n.feedkeys(n.replace_termcodes("<C-o>"), 'xt')
assert (n.api.get_mode() == dict(
    {'mode': 's', 'blocking': False})), "not in select mode or is blocked!"
assert (n.api.get_mode() == dict(
    {'mode': 's', 'blocking': False})), "not in select mode or is blocked!"

n.feedkeys(n.replace_termcodes("<C-o>"), 't')
assert (n.api.get_mode() == dict(
    {'mode': 'vs', 'blocking': False})), "not in select visual mode or is blocked!"
assert (n.api.get_mode() == dict(
    {'mode': 'vs', 'blocking': False})), "not in select visual mode or is blocked!"

n.close()
