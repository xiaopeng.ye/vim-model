import neovim
from aalpy.base import SUL
from aalpy.automata import MooreMachine
from aalpy.oracles import StatePrefixEqOracle
from aalpy.learning_algs import run_Lstar
from aalpy.utils import visualize_automaton, save_automaton_to_file
from random import seed
seed(100)  # all experiments will be reproducible


class MooreSUL(SUL):
    """
    System under learning for Moore machine
    """

    def __init__(self):
        super().__init__()
        self.cast_mode = {'n': 'Normal', 'no': 'Operator-pending', 'nov': 'Operator-pending charwise',
                          'noV': 'Operator-pending linewise', 'niI': 'Insert Normal', 'ix': 'Insert Ctrl-X Mode',
                          'i': 'Insert', 'c': 'Command-line editting', 'v': 'Visual', 'V': 'Visual Line',
                          '\x16': 'Visual Block', 'ic': 'Insert Command-line completion', 's': 'Select', 'S': 'Select Line',
                          '\x13': 'Select Block', 'R': 'Replace', 'Rv': 'Virtual Replace', 'no\x16': 'Operator-pending blockwise',
                          'niV': 'Virtual Replace Normal', 'niR': 'Replace Normal', 'Rx': 'Replace Ctrl-X Mode', 'Rc': 'Replace Command-line completion',
                          'r': 'Hit-enter prompt', 'cv': 'Ex mode', 'rm': 'The more prompt', 't': 'Terminal',
                          # new modes added by vim on the path 8.2.3236
                          'vs': 'Select Visual', 'Vs': 'Select Visual Line', '\x16s': 'Select Visual Block', 'nt': 'Normal in terminal-emulator', 't': 'Terminal mode'}

        # self.n = neovim.attach(
        #     "child", argv=['/bin/env', 'nvim', '--clean', '--embed', '--headless'])
        #self.n = neovim.attach(
        #    "child", argv=['nvim', '--clean', '--embed', '--headless', 'filename'])
        # self.n.input(":set ttm=10<CR>")
        # self.n.input(":set complete=.<CR>")
        #self.n.command("lua vim.api.nvim_set_keymap('n', '<C-O>', '', {})")
        #self.n.exec_lua(""" vim.api.nvim_set_keymap('n', '<C-I>', '', {}) """)
        #self.n.lua.vim.api.nvim_set_keymap('n', '<C-O>', '', {})
        #self.n.lua.vim.api.nvim_set_keymap('n', '<C-I>', '', {})
        #self.n.vars['cmdheight']=2 #("set cmdheight=2")

    def pre(self):
        """ """
        print("pre: ", end='')
        #print(self.cast_mode[self.n.api.get_mode()["mode"]])
        # assert (self.n.api.get_mode() == dict(
        #     {'mode': 'n', 'blocking': False})), "not in normal mode or is blocked!"
        # buffer = self.n.current.buffer  # Get the current buffer
        # assert (buffer.api.line_count() == 1), "non empty buffer"
        # # print('pre')
        self.n = neovim.attach(
            "child", argv=['nvim', '--clean', '--embed', '--headless', 'filename'])
        self.n.lua.vim.api.nvim_set_keymap('n', '<C-O>', '', {})
        self.n.lua.vim.api.nvim_set_keymap('n', '<C-I>', '', {})
        self.n.vars['cmdheight']=2
        # self.n.input(":set ttm=10<CR>")
        # self.n.input(":set complete=.<CR>")
        # self.n.input(":set shortmess=a<CR>")
        # We do not want those because they mess up with the state machine construction by possibly jumping to other windows
        #self.n.input(":lua vim.api.nvim_set_keymap('n', '<C-O>', '', {})")
        #self.n.input(":lua vim.api.nvim_set_keymap('n', '<C-I>', '', {})")
        #self.n.input(":set cmdheight=2<CR>")
        #self.n.feedkeys(self.n.replace_termcodes("<C-c>"))
        #self.n.feedkeys(self.n.replace_termcodes("<C-\><C-N>"))
        #self.n.feedkeys(self.n.replace_termcodes("<C-c>"), 'xt')
        #self.n.feedkeys(self.n.replace_termcodes("<C-\><C-N>"), 'xt')
        #self.n.feedkeys(":%bwipeout!<CR>", 'xt')
        #self.n.input(":%bwipeout!<CR>")
        #self.n.command('%bwipeout!')
        #sleep(.10)
        print("end pre: ", end='')
        print(self.cast_mode[self.n.api.get_mode()["mode"]])

    def post(self):
        print("post: ", end='')
        self.n.close()
        # if self.n.api.get_mode()['mode'] == 'ce':
        #     self.n.input('vi\n')
        # if self.n.api.get_mode()["blocking"] is True:
        #     print("Input ended up blocking, feeding unblocking sequence")
        #     self.n.input("<C-\><C-N>")
        #     self.n.input("<Esc>")
        #     self.n.input("<C-c>")
        #     self.n.input("<Esc>")
        # print('post')

    def step(self, letter):
        current_mode = self.n.api.get_mode()
        if letter is not None:
            if current_mode["blocking"] is True:
                # print(current_mode["mode"])
                # print(letter)
                self.n.input(letter)
            elif current_mode["mode"] in ('ce'):
                # elif current_mode["mode"] in ('ix', 'Rx', 'ce'):
                return self.cast_mode[current_mode["mode"]]
            # elif current_mode["mode"] in ('no', 'nov', 'noV', 'no\x16') and letter not in ('<Esc>', '<C-c>', 'v', '<C-v>', 'V'):
            #     return self.cast_mode[current_mode["mode"]]
            elif letter == '<C-o>' and current_mode["mode"] == 's':
                # print("<C-o>@s")
                self.n.feedkeys(self.n.replace_termcodes('<C-o>'), 't')
            elif letter == '<C-o>' and current_mode["mode"] == 'i':
                # print("<C-o>@i")
                self.n.feedkeys(self.n.replace_termcodes('<C-o>'), 'L')
            elif letter == '<C-o>' and current_mode["mode"] == 'v':
                # print("<C-o>@v")
                self.n.feedkeys(self.n.replace_termcodes('<C-o>'), 'Lx')
            elif letter == '<C-o>' and current_mode["mode"] == 'vs':
                # print("<C-o>@v")
                self.n.feedkeys(self.n.replace_termcodes('<C-o>'), 'xt')
            elif letter == 'gh':
                # print("gh", end='')
                self.n.feedkeys(self.n.replace_termcodes('gh'), 't')
            elif letter == 'vi':
                if current_mode["mode"] == 'ce':
                    self.n.input('vi<CR>')
                else:
                    return self.cast_mode[current_mode["mode"]]
            else:
                print(letter, end='')
                self.n.feedkeys(self.n.replace_termcodes(letter), 't')

        next_mode = self.n.api.get_mode()

        if next_mode["blocking"] is True:
            if next_mode['mode'] == current_mode['mode'] and next_mode['mode'] in ('i', 'R'):
                return self.cast_mode[next_mode['mode']] + ' Special character pending'

        # print(current_mode)
        # print(letter)
        # print(next_mode)
        print('@'+self.cast_mode[next_mode['mode']])
        return self.cast_mode[next_mode["mode"]]


sul = MooreSUL()

input_al = ['i', 'vi<CR>', 'Q', '<C-\><C-N>', ':term<CR>', '<C-D>', 'c', ':', 'V', 'gh', '<C-o>', 'vi', 'R', '<Esc>']

# input_al = ['i', ':', 'v', 'V', '<C-v>', '<C-q>',
# '<C-x>', '<Esc>', 'gh', '<C-o>', 'Q', 'vi', 'R', 'c']

# input_al = ['R', 'gR', 'i', '<C-x>', 'gr', '<Esc>', 'r']

# input_al = ['c', '<Esc>', '<C-c>', 'i', ':', 'v', '<C-v>', 'V']

# input_al = ['i', 'vi', ':', 'v', '<C-x>', 'R', 'Q', '<C-v>']
# Solved with --es flag and self.n.input(":set cmdheight=2<CR>")
# Non-determinism detected.
# Error inserting: ['i', '<C-x>', '<C-v>']
# Conflict detected: Insert Command-line completion vs Hit-enter prompt
# Expected Output: ['Insert', 'Insert Ctrl-X Mode', 'Insert Command-line completion']
# Received output: ['Insert', 'Insert Ctrl-X Mode', 'Hit-enter prompt']

# input_al = ['i', '<Esc>', 'gh', 'v', '<C-o>']  # Conflict solved

# input_al = ['i', 'vi', '<C-v>', ':', 'v', 'R', 'Q'] #vi input solved

# input_al = ['i', ':', 'v', 'V', '<C-v>', '<C-q>', '<C-x>']

state_origin_eq_oracle = StatePrefixEqOracle(
    input_al, sul, walks_per_state=15, walk_len=10)
learned_moore = run_Lstar(input_al, sul, state_origin_eq_oracle, cex_processing='rs',
                          closing_strategy='single', automaton_type='moore', cache_and_non_det_check=True, print_level=3)

# visualize_automaton(learned_moore, file_type='svg')

save_automaton_to_file(learned_moore, path='nvim-moore', file_type='dot')
